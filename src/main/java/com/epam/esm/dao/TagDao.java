package com.epam.esm.dao;

import com.epam.esm.models.Gift;
import com.epam.esm.models.Tag;
import java.util.List;

public interface TagDao {
  Tag insert(Tag tag);

  void delete(Tag tag);

  Tag findById(int id);

  List<Tag> findAll();

  void associate(Integer tagId, Integer GiftId);

  List<Tag> findByGiftId(Integer gift_id);

  void deleteAssociationByGift(Gift gift);

  void deleteAssociationByTag(Tag tag);
}
