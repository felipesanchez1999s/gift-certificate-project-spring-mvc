package com.epam.esm.dao;

import com.epam.esm.models.Gift;
import java.util.List;

public interface GiftDAO {
  Gift insert(Gift gift);

  void delete(Gift gift);

  Gift findById(Integer id);

  List<Gift> findAll();

  Gift update(Gift gift);

  List<Gift> findByTagName(String tagName);
}
