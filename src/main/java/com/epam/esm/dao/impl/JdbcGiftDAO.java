package com.epam.esm.dao.impl;

import com.epam.esm.dao.GiftDAO;
import com.epam.esm.models.Gift;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

// @Component
public class JdbcGiftDAO implements GiftDAO {
  private static final String INSERT_SQL =
      "INSERT INTO gift_certificate (gift_id, name,description,price,duration, create_date,last_update_date) VALUES (?, ?, ?, ?, ?, ?, ?)";
  private static final String DELETE_SQL = "DELETE FROM gift_certificate WHERE gift_id=?";
  private static final String SELECT_ONE_SQL = "SELECT * FROM gift_certificate WHERE gift_id=?";
  private static final String SELECT_ALL_SQL = "SELECT * FROM gift_certificate";
  private static final String UPDATE =
      "UPDATE gift_certificate set name=?,description=?,price=?,duration=?,last_update_date=? where gift_id=?";

  private static final String SELECT_BY_TAG_NAME =
      "SELECT * FROM gift_certificate LEFT JOIN gift_tag on gift_tag.gift_id=gift_certificate.gift_id LEFT JOIN tag ON tag.tag_id=gift_tag.tag_id WHERE tag.name = ?";
  private final DataSource dataSource;

  public JdbcGiftDAO(DataSource dataSource) {

    this.dataSource = dataSource;
  }

  @Override
  public Gift insert(Gift gift) {
    Gift inserted = null;
    try (Connection conn = dataSource.getConnection();
        PreparedStatement ps = conn.prepareStatement(INSERT_SQL)) {
      ps.setInt(1, gift.getId());
      ps.setString(2, gift.getName());
      ps.setString(3, gift.getDescription());
      ps.setBigDecimal(4, gift.getPrice());
      ps.setInt(5, gift.getDuration());

      ps.setTimestamp(6, gift.getCreateDate());
      ps.setTimestamp(7, gift.getLastUpdateDate());
      ps.executeUpdate();
      inserted = gift;
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
    return inserted;
  }

  @Override
  public void delete(Gift gift) {
    try (Connection conn = dataSource.getConnection();
        PreparedStatement ps = conn.prepareStatement(DELETE_SQL)) {
      ps.setInt(1, gift.getId());
      ps.executeUpdate();
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public Gift findById(Integer id) {
    Gift gift = null;
    try (Connection conn = dataSource.getConnection();
        PreparedStatement ps = conn.prepareStatement(SELECT_ONE_SQL)) {
      ps.setInt(1, id);

      try (ResultSet rs = ps.executeQuery()) {
        if (rs.next()) gift = toGift(rs);
      }
      return gift;
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public List<Gift> findAll() {

    try (Connection conn = dataSource.getConnection();
        PreparedStatement ps = conn.prepareStatement(SELECT_ALL_SQL);
        ResultSet rs = ps.executeQuery()) {

      List<Gift> gifts = new ArrayList<>();
      while (rs.next()) {
        gifts.add(toGift(rs));
      }
      return gifts;
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public Gift update(Gift gift) {
    Gift updated = null;
    try (Connection conn = dataSource.getConnection();
        PreparedStatement ps = conn.prepareStatement(UPDATE)) {
      ps.setString(1, gift.getName());
      ps.setString(2, gift.getDescription());
      ps.setBigDecimal(3, gift.getPrice());

      ps.setInt(4, gift.getDuration());

      ps.setTimestamp(5, gift.getLastUpdateDate());
      ps.setInt(6, gift.getId());
      ps.executeUpdate();
      updated = gift;
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
    return updated;
  }

  @Override
  public List<Gift> findByTagName(String tagName) {

    try (Connection conn = dataSource.getConnection();
        PreparedStatement ps = conn.prepareStatement(SELECT_BY_TAG_NAME); ) {
      ps.setString(1, tagName);
      ResultSet rs = ps.executeQuery();

      List<Gift> gifts = new ArrayList<>();
      while (rs.next()) {
        gifts.add(toGift(rs));
      }
      return gifts;
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  private Gift toGift(ResultSet rs) throws SQLException {
    Gift gift = new Gift();
    gift.setId(rs.getInt("gift_id"));
    gift.setName(rs.getString("name"));
    gift.setDescription(rs.getString("description"));

    gift.setPrice(rs.getBigDecimal("price"));
    gift.setDuration(rs.getInt("duration"));
    gift.setCreateDate(rs.getTimestamp("create_date"));
    gift.setLastUpdateDate(rs.getTimestamp("last_update_date"));

    return gift;
  }
}
