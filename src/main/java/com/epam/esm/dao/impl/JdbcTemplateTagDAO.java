package com.epam.esm.dao.impl;

import com.epam.esm.dao.TagDao;
import com.epam.esm.models.Gift;
import com.epam.esm.models.Tag;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

public class JdbcTemplateTagDAO implements TagDao {

  private static final String INSERT_SQL = "INSERT INTO tag (tag_id, name) VALUES (?, ?)";
  private static final String DELETE_SQL = "DELETE FROM tag WHERE tag_id=?";
  private static final String SELECT_ONE_SQL = "SELECT * FROM tag WHERE tag_id=?";
  private static final String SELECT_ALL_SQL = "SELECT * FROM tag";

  private static final String INSERT_ASSOCIATION =
      "INSERT INTO gift_tag (gift_id,tag_id) VALUES (?, ?)";

  private static final String SELECT_BY_GIFT_ID =
      "SELECT * FROM tag LEFT JOIN gift_tag on gift_tag.tag_id=tag.tag_id WHERE gift_tag.gift_id = ?";

  private static final String DELETE_ASSOCIATION_BY_GIFT = "DELETE FROM gift_tag where gift_id=?";
  private static final String DELETE_ASSOCIATION_BY_TAG = "DELETE FROM gift_tag where tag_id=?";

  private JdbcTemplate jdbcTemplate;

  public JdbcTemplateTagDAO(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  @Override
  public Tag insert(Tag tag) {

    Tag insertedTag = null;
    try {
      jdbcTemplate.update(INSERT_SQL, tag.getId(), tag.getName());
      insertedTag = tag;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    return insertedTag;
  }

  @Override
  public void delete(Tag tag) {

    try {
      jdbcTemplate.update(DELETE_SQL, tag.getId());
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public Tag findById(int id) {

    try {
      Tag tag =
          jdbcTemplate.query(
              SELECT_ONE_SQL,
              new ResultSetExtractor<Tag>() {
                @Override
                public Tag extractData(ResultSet rs) throws SQLException, DataAccessException {
                  Tag tag = null;

                  if (rs.next()) tag = toTag(rs);

                  return tag;
                }
              },
              id);
      return tag;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public List<Tag> findAll() {

    try {
      List<Tag> tags =
          jdbcTemplate.query(
              SELECT_ALL_SQL,
              new ResultSetExtractor<List<Tag>>() {
                @Override
                public List<Tag> extractData(ResultSet rs)
                    throws SQLException, DataAccessException {
                  List<Tag> tags = new ArrayList<>();
                  while (rs.next()) {
                    tags.add(toTag(rs));
                  }
                  return tags;
                }
              });
      return tags;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void associate(Integer tagId, Integer giftId) {

    try {
      jdbcTemplate.update(INSERT_ASSOCIATION, giftId, tagId);

    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public List<Tag> findByGiftId(Integer gift_id) {

    try {
      List<Tag> tags =
          jdbcTemplate.query(
              SELECT_BY_GIFT_ID,
              new ResultSetExtractor<List<Tag>>() {
                @Override
                public List<Tag> extractData(ResultSet rs)
                    throws SQLException, DataAccessException {
                  List<Tag> tags = new ArrayList<>();

                  while (rs.next()) tags.add(toTag(rs));

                  return tags;
                }
              },
              gift_id);
      return tags;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void deleteAssociationByGift(Gift gift) {

    try {
      jdbcTemplate.update(DELETE_ASSOCIATION_BY_GIFT, gift.getId());
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void deleteAssociationByTag(Tag tag) {

    try {
      jdbcTemplate.update(DELETE_ASSOCIATION_BY_TAG, tag.getId());
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private Tag toTag(ResultSet rs) throws SQLException {
    Tag tag = new Tag();
    tag.setId(rs.getInt("tag_id"));
    tag.setName(rs.getString("name"));
    return tag;
  }
}
