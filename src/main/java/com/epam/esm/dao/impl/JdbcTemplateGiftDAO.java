package com.epam.esm.dao.impl;

import com.epam.esm.dao.GiftDAO;
import com.epam.esm.models.Gift;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

@Component
public class JdbcTemplateGiftDAO implements GiftDAO {
    private JdbcTemplate jdbcTemplate;
    private static final String INSERT_SQL = "INSERT INTO gift_certificate (gift_id, name,description,price,duration, create_date,last_update_date) VALUES (?, ?, ?, ?, ?, ?, ?)";
    private static final String DELETE_SQL = "DELETE FROM gift_certificate WHERE gift_id=?";
    private static final String SELECT_ONE_SQL = "SELECT * FROM gift_certificate WHERE gift_id=?";
    private static final String SELECT_ALL_SQL = "SELECT * FROM gift_certificate";
    private static final String UPDATE="UPDATE gift_certificate set name=?,description=?,price=?,duration=?,last_update_date=? where gift_id=?";

    private static final String SELECT_BY_TAG_NAME="SELECT * FROM gift_certificate LEFT JOIN gift_tag on gift_tag.gift_id=gift_certificate.gift_id LEFT JOIN tag ON tag.tag_id=gift_tag.tag_id WHERE tag.name = ?";

  public JdbcTemplateGiftDAO(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

    @Override
    public Gift insert(Gift gift) {
        Gift inserted =null;
        try{
            jdbcTemplate.update(
                    INSERT_SQL, gift.getId(), gift.getName(),gift.getDescription(), gift.getPrice(),gift.getDuration(),gift.getCreateDate(),gift.getCreateDate());
            inserted=gift;
        }catch (Exception e){
            throw  new RuntimeException(e);
        }
        return inserted;
    }

    @Override
    public void delete(Gift gift) {
        try {
            jdbcTemplate.update(DELETE_SQL,gift.getId());
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Gift findById(Integer id) {
        Gift gift = null;
        try{
            gift=jdbcTemplate.query(SELECT_ONE_SQL, new ResultSetExtractor<Gift>() {
                @Override
                public Gift extractData(ResultSet rs) throws SQLException, DataAccessException {
                    Gift returnedGift=null;
                    if (rs.next())
                        returnedGift = toGift(rs);
                    return returnedGift;
                }
            },id);
            return gift;
        }catch (Exception e){
            throw new RuntimeException(e);
        }

    }

    @Override
    public List<Gift> findAll() {
        

        try  {

            List<Gift> gifts =     jdbcTemplate.query(SELECT_ALL_SQL, new ResultSetExtractor<List<Gift>>() {
                @Override
                public List<Gift> extractData(ResultSet rs) throws SQLException, DataAccessException {
                    List<Gift> gifts = new ArrayList<>();
                    while (rs.next()) {
                        gifts.add(toGift(rs));
                    }
                    return gifts;
                }
            });
            return gifts;
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Gift update(Gift gift) {


        Gift updated =null;
        try  {
            jdbcTemplate.update(UPDATE,gift.getName(),gift.getDescription(),gift.getPrice(),gift.getDuration(),gift.getLastUpdateDate(),gift.getId());
            updated=gift;
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return updated;
    }

    @Override
    public List<Gift> findByTagName(String tagName) {
        

        try{
            List<Gift>gifts=jdbcTemplate.query(SELECT_BY_TAG_NAME, new ResultSetExtractor<List<Gift>>() {
                @Override
                public List<Gift> extractData(ResultSet rs) throws SQLException, DataAccessException {
                    List<Gift> gifts = new ArrayList<>();
                    while (rs.next()) {
                        gifts.add(toGift(rs));
                    }
                    return gifts;
                }
            },tagName);
            return gifts;
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    private Gift toGift(ResultSet rs) throws SQLException{
        Gift gift = new Gift();
        gift.setId(rs.getInt("gift_id"));
        gift.setName(rs.getString("name"));
        gift.setDescription(rs.getString("description"));

        gift.setPrice(rs.getBigDecimal("price"));
        gift.setDuration(rs.getInt("duration"));
        gift.setCreateDate(rs.getTimestamp("create_date"));
        gift.setLastUpdateDate(rs.getTimestamp("last_update_date"));

        return gift;
    }
}
