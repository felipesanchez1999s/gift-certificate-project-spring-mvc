package com.epam.esm.dao.impl;

import com.epam.esm.dao.TagDao;
import com.epam.esm.models.Gift;
import com.epam.esm.models.Tag;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

public class JdbcTagDao implements TagDao {
  private static final String INSERT_SQL = "INSERT INTO tag (tag_id, name) VALUES (?, ?)";
  private static final String DELETE_SQL = "DELETE FROM tag WHERE tag_id=?";
  private static final String SELECT_ONE_SQL = "SELECT * FROM tag WHERE tag_id=?";
  private static final String SELECT_ALL_SQL = "SELECT * FROM tag";

  private static final String INSERT_ASSOCIATION =
      "INSERT INTO gift_tag (gift_id,tag_id) VALUES (?, ?)";

  private static final String SELECT_BY_GIFT_ID =
      "SELECT * FROM tag LEFT JOIN gift_tag on gift_tag.tag_id=tag.tag_id WHERE gift_tag.gift_id = ?";

  private static final String DELETE_ASSOCIATION_BY_GIFT = "DELETE FROM gift_tag where gift_id=?";
  private static final String DELETE_ASSOCIATION_BY_TAG = "DELETE FROM gift_tag where tag_id=?";

  private final DataSource dataSource;

  public JdbcTagDao(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  @Override
  public Tag insert(Tag tag) {
    Tag insertedTag = null;
    try (Connection conn = dataSource.getConnection();
        PreparedStatement ps = conn.prepareStatement(INSERT_SQL)) {
      prepareStatement(ps, tag);
      ps.executeUpdate();
      insertedTag = tag;
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
    return insertedTag;
  }

  @Override
  public void delete(Tag tag) {
    try (Connection conn = dataSource.getConnection();
        PreparedStatement ps = conn.prepareStatement(DELETE_SQL)) {
      ps.setInt(1, tag.getId());
      ps.executeUpdate();
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public Tag findById(int id) {
    try (Connection conn = dataSource.getConnection();
        PreparedStatement ps = conn.prepareStatement(SELECT_ONE_SQL)) {
      ps.setInt(1, id);
      Tag tag = null;
      try (ResultSet rs = ps.executeQuery()) {
        if (rs.next()) tag = toTag(rs);
      }
      return tag;
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public List<Tag> findAll() {
    try (Connection conn = dataSource.getConnection();
        PreparedStatement ps = conn.prepareStatement(SELECT_ALL_SQL);
        ResultSet rs = ps.executeQuery()) {

      List<Tag> tags = new ArrayList<>();
      while (rs.next()) {
        tags.add(toTag(rs));
      }
      return tags;
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void associate(Integer tagId, Integer giftId) {

    try (Connection conn = dataSource.getConnection();
        PreparedStatement ps = conn.prepareStatement(INSERT_ASSOCIATION); ) {

      ps.setInt(1, giftId);
      ps.setInt(2, tagId);
      ps.executeUpdate();

    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public List<Tag> findByGiftId(Integer gift_id) {
    try (Connection conn = dataSource.getConnection();
        PreparedStatement ps = conn.prepareStatement(SELECT_BY_GIFT_ID)) {
      ps.setInt(1, gift_id);
      List<Tag> tags = new ArrayList<>();
      try (ResultSet rs = ps.executeQuery()) {
        while (rs.next()) tags.add(toTag(rs));
      }
      return tags;
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void deleteAssociationByGift(Gift gift) {
    try (Connection conn = dataSource.getConnection();
        PreparedStatement ps = conn.prepareStatement(DELETE_ASSOCIATION_BY_GIFT)) {
      ps.setInt(1, gift.getId());
      ps.executeUpdate();
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void deleteAssociationByTag(Tag tag) {
    try (Connection conn = dataSource.getConnection();
        PreparedStatement ps = conn.prepareStatement(DELETE_ASSOCIATION_BY_TAG)) {
      ps.setInt(1, tag.getId());
      ps.executeUpdate();
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  private Tag toTag(ResultSet rs) throws SQLException {
    Tag tag = new Tag();
    tag.setId(rs.getInt("tag_id"));
    tag.setName(rs.getString("name"));
    return tag;
  }

  private void prepareStatement(PreparedStatement ps, Tag tag) throws SQLException {
    ps.setInt(1, tag.getId());
    ps.setString(2, tag.getName());
  }
}
