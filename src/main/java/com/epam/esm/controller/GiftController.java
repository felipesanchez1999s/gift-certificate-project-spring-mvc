package com.epam.esm.controller;

import com.epam.esm.DTO.GiftDTO;
import com.epam.esm.models.Gift;
import com.epam.esm.service.GiftService;
import com.epam.esm.service.TagService;
import com.epam.esm.utils.DateUtils;
import com.epam.esm.utils.Mapper;
import com.epam.esm.utils.ValidationObject;
import java.io.IOException;
import java.time.*;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.*;

/** Controller for handling gift-related operations. */
@Controller
@Transactional
public class GiftController {

  private GiftService giftService;
  private TagService tagService;
  private TransactionTemplate transactionTemplate;

  public GiftController(
      GiftService giftService, TagService tagService, TransactionTemplate transactionTemplate) {
    this.giftService = giftService;
    this.tagService = tagService;
    this.transactionTemplate = transactionTemplate;
  }

  /**
   * Retrieves all gifts.
   *
   * @param response the HTTP servlet response
   * @return the list of gift DTOs wrapped in a ResponseEntity
   * @throws IOException if an I/O exception occurs
   */
  @RequestMapping(
      value = "/gift",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public @ResponseBody ResponseEntity getGift(HttpServletResponse response) throws IOException {
    List<GiftDTO> giftDTOList = giftService.findAll();
    giftDTOList.stream()
        .forEach(
            giftDTO -> {
              giftDTO.setTags(
                  tagService.findByGiftId(giftDTO.getId()).stream()
                      .map(tag -> Mapper.toTagDTO(tag))
                      .collect(Collectors.toList()));
            });
    return ResponseEntity.ok(giftDTOList);
  }
  /**
   * Retrieves gifts based on the provided query parameters.
   *
   * @param response the HTTP servlet response
   * @param tagName the tag name query parameter
   * @param partialName the partial name query parameter
   * @param partialDescription the partial description query parameter
   * @param sortField the sort field query parameter
   * @param order the sort order query parameter
   * @return the list of filtered gift DTOs wrapped in a ResponseEntity
   * @throws IOException if an I/O exception occurs
   */
  @RequestMapping(
      value = "/gift/query",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public @ResponseBody ResponseEntity getGift(
      HttpServletResponse response,
      @RequestParam(required = false) String tagName,
      @RequestParam(required = false) String partialName,
      @RequestParam(required = false) String partialDescription,
      @RequestParam(required = false) String sortField,
      @RequestParam(required = false) String order)
      throws IOException {

    List<GiftDTO> giftDTOList =
        giftService.findByParams(tagName, partialName, partialDescription, sortField, order);

    return ResponseEntity.ok(giftDTOList);
  }
  /**
   * Updates a gift.
   *
   * @param giftDTO the gift DTO containing the updated information
   * @return the updated gift wrapped in a ResponseEntity
   */
  @RequestMapping(value = "/gift", method = RequestMethod.PUT)
  @Transactional
  public @ResponseBody ResponseEntity updateGift(@RequestBody GiftDTO giftDTO) {
    ValidationObject validGift = giftService.isRigthForUpdate(giftDTO);
    if (!validGift.isValid()) throw new IllegalArgumentException(validGift.getMessage());

    if (!giftService.exist(giftDTO.getId()))
      throw new IllegalStateException("A gift with the specified id doesnt exists");

    Gift giftUpdated = giftService.update(giftDTO);

    giftDTO.getTags().stream()
        .forEach(
            tagDTO -> {
              ValidationObject validTag = tagService.isRightTag(tagDTO);
              if (!validTag.isValid()) throw new IllegalArgumentException(validTag.getMessage());
              if (!tagService.exists(tagDTO.getId())) tagService.save(Mapper.toTag(tagDTO));
              if (!tagService.findByGiftId(giftDTO.getId()).stream()
                  .anyMatch(tag -> tag.getId() == tagDTO.getId()))
                tagService.associate(tagDTO.getId(), giftDTO.getId());
            });

    return ResponseEntity.ok(giftUpdated);
  }

  /**
   * Creates a new gift.
   *
   * @param giftDTO the gift DTO containing the information of the new gift
   * @return the created gift DTO wrapped in a ResponseEntity
   * @throws IOException if an I/O exception occurs
   */
  @RequestMapping(value = "/gift", method = RequestMethod.POST)
  @Transactional
  public @ResponseBody ResponseEntity postGift(@RequestBody GiftDTO giftDTO) throws IOException {

    ValidationObject validGift = giftService.isRightGift(giftDTO);
    if (!validGift.isValid()) throw new IllegalArgumentException(validGift.getMessage());

    if (giftService.exist(giftDTO.getId()))
      throw new IllegalStateException("A gift with the specified id already exists");

    Gift newGift = Mapper.toGift(giftDTO);
    Gift registeredGift = giftService.save(newGift);
    giftDTO.setCreatedDate(DateUtils.getIsoDate(registeredGift.getCreateDate()));
    giftDTO.setLastUpdateDate(DateUtils.getIsoDate(registeredGift.getLastUpdateDate()));

    giftDTO.getTags().stream()
        .forEach(
            tagDTO -> {
              ValidationObject validTag = tagService.isRightTag(tagDTO);
              if (!validTag.isValid()) throw new IllegalArgumentException(validTag.getMessage());
              if (!tagService.exists(tagDTO.getId())) tagService.save(Mapper.toTag(tagDTO));

              tagService.associate(tagDTO.getId(), giftDTO.getId());
            });

    return ResponseEntity.ok(giftDTO);
  }

  /**
   * Deletes a gift by its ID.
   *
   * @param giftId the ID of the gift to be deleted
   * @return the deleted gift wrapped in a ResponseEntity
   */
  @RequestMapping(value = "/gift/{gift_id}", method = RequestMethod.DELETE)
  @Transactional
  public @ResponseBody ResponseEntity deleteGift(@PathVariable Integer gift_id) {
    Gift gift = giftService.findyById(gift_id);
    if (gift == null) throw new IllegalStateException("gift to delete doesn't exist");
    giftService.delete(gift);
    return ResponseEntity.ok(gift);
  }
}
