package com.epam.esm.controller;

import com.epam.esm.DTO.TagDTO;
import com.epam.esm.models.Tag;
import com.epam.esm.service.TagService;
import com.epam.esm.utils.Mapper;
import com.epam.esm.utils.ValidationObject;
import java.io.IOException;
import java.util.List;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@Controller
@Transactional
public class TagController {

  private TagService tagService;

  public TagController(TagService tagService) {
    this.tagService = tagService;
  }

  /**
   * Retrieves all tags.
   *
   * @return A ResponseEntity containing a list of tags.
   */
  @RequestMapping(
      value = "/tag",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public @ResponseBody ResponseEntity getTags() {

    List<Tag> tags = tagService.findAll();

    return ResponseEntity.ok(tags);
  }
  /**
   * Creates a new tag.
   *
   * @param tagDTO The DTO object representing the tag to be created.
   * @return A ResponseEntity containing the created tag.
   * @throws IOException if an I/O error occurs.
   */
  @RequestMapping(value = "/tag", method = RequestMethod.POST)
  @Transactional
  public @ResponseBody ResponseEntity postGift(@RequestBody TagDTO tagDTO) throws IOException {

    ValidationObject validGift = tagService.isRightTag(tagDTO);
    if (!validGift.isValid()) throw new IllegalArgumentException(validGift.getMessage());

    if (tagService.exists(tagDTO.getId()))
      throw new IllegalStateException("A tag with the specified id already exists");
    Tag newTag = Mapper.toTag(tagDTO);
    tagService.save(newTag);
    return ResponseEntity.ok(newTag);
  }

  /**
   * Deletes a tag by its ID.
   *
   * @param tag_id The ID of the tag to be deleted.
   * @return A ResponseEntity containing the deleted tag.
   */
  @RequestMapping(value = "/tag/{tag_id}", method = RequestMethod.DELETE)
  @Transactional
  public @ResponseBody ResponseEntity deleteTag(@PathVariable Integer tag_id) {
    Tag tag = tagService.findById(tag_id);
    if (tag == null) throw new IllegalStateException("tag to delete doesn't exist");
    tagService.delete(tag);
    return ResponseEntity.ok(tag);
  }
}
