package com.epam.esm.error;

import com.epam.esm.utils.ErrorBody;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(value = {IllegalArgumentException.class})
  protected ResponseEntity handleIllegalArgumentException(
      IllegalArgumentException ex, WebRequest request) {

    ErrorBody errorBody = new ErrorBody();
    errorBody.setErrorCode(HttpStatus.BAD_REQUEST.value());
    errorBody.setErrorMessage(ex.getMessage());
    return handleExceptionInternal(
        ex, errorBody, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
  }

  @ExceptionHandler(value = {IllegalStateException.class})
  protected ResponseEntity handleIllegalArgumentException(
      IllegalStateException ex, WebRequest request) {

    ErrorBody errorBody = new ErrorBody();
    errorBody.setErrorCode(HttpStatus.CONFLICT.value());
    errorBody.setErrorMessage(ex.getMessage());
    return handleExceptionInternal(ex, errorBody, new HttpHeaders(), HttpStatus.CONFLICT, request);
  }
}
