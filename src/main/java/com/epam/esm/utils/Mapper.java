package com.epam.esm.utils;

import com.epam.esm.DTO.GiftDTO;
import com.epam.esm.DTO.TagDTO;
import com.epam.esm.models.Gift;
import com.epam.esm.models.Tag;

public class Mapper {

  public static Gift toGift(GiftDTO giftDTO) {
    Gift newGift = new Gift();
    newGift.setId(giftDTO.getId());
    newGift.setName(giftDTO.getName());
    newGift.setDescription(giftDTO.getDescription());
    newGift.setPrice(giftDTO.getPrice());
    newGift.setDuration(giftDTO.getDuration());

    return newGift;
  }

  public static GiftDTO toGiftDTO(Gift gift) {
    GiftDTO giftDTO = new GiftDTO();
    giftDTO.setId(gift.getId());
    giftDTO.setName(gift.getName());
    giftDTO.setDescription(gift.getDescription());
    giftDTO.setDuration(gift.getDuration());
    giftDTO.setPrice(gift.getPrice());
    giftDTO.setLastUpdateDate(DateUtils.getIsoDate(gift.getLastUpdateDate()));
    giftDTO.setCreatedDate(DateUtils.getIsoDate(gift.getCreateDate()));
    return giftDTO;
  }

  public static Tag toTag(TagDTO tagDTO) {
    Tag newTag = new Tag();
    newTag.setId(tagDTO.getId());
    newTag.setName(tagDTO.getName());
    return newTag;
  }

  public static TagDTO toTagDTO(Tag tag) {
    TagDTO tagDTO = new TagDTO();
    tagDTO.setId(tag.getId());
    tagDTO.setName(tag.getName());
    return tagDTO;
  }
}
