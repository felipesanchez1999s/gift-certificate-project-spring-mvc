package com.epam.esm.utils;

public class ValidationObject {

  private String message;
  private boolean valid;

  public ValidationObject(String message, boolean valid) {
    this.message = message;
    this.valid = valid;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public boolean isValid() {
    return valid;
  }

  public void setValid(boolean valid) {
    this.valid = valid;
  }
}
