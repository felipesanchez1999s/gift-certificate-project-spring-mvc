package com.epam.esm.service;

import com.epam.esm.DTO.TagDTO;
import com.epam.esm.models.Tag;
import com.epam.esm.utils.ValidationObject;
import java.util.List;

public interface TagService {
  public boolean exists(Integer id);

  public Tag save(Tag newTag);

  ValidationObject isRightTag(TagDTO tagDTO);

  public void associate(Integer tagId, Integer giftId);

  public List<Tag> findByGiftId(Integer gift_id);

  public List<Tag> findAll();

  public Tag findById(Integer tag_id);

  public void delete(Tag tag);
}
