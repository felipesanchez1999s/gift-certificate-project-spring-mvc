package com.epam.esm.service;

import com.epam.esm.DTO.GiftDTO;
import com.epam.esm.models.Gift;
import com.epam.esm.utils.ValidationObject;
import java.util.List;

public interface GiftService {
  public Gift save(Gift newGiftDTO);

  public boolean exist(Integer gift_id);

  public ValidationObject isRightGift(GiftDTO giftDTO);

  public ValidationObject isRigthForUpdate(GiftDTO giftDTO);

  public List<GiftDTO> findAll();

  public Gift findyById(Integer id);

  public Gift update(GiftDTO giftDTO);

  public void delete(Gift giftDTO);

  public List<GiftDTO> findByParams(
      String tagName,
      String partialName,
      String partialDescription,
      String sortField,
      String order);
}
