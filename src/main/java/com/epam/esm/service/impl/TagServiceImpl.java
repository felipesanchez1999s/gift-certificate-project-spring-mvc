package com.epam.esm.service.impl;

import com.epam.esm.DTO.TagDTO;
import com.epam.esm.dao.TagDao;
import com.epam.esm.models.Tag;
import com.epam.esm.service.TagService;
import com.epam.esm.utils.ValidationObject;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class TagServiceImpl implements TagService {

  private TagDao tagDao;

  public TagServiceImpl(TagDao tagDao) {
    this.tagDao = tagDao;
  }

  @Override
  public boolean exists(Integer id) {
    return tagDao.findById(id) != null;
  }

  @Override
  public Tag save(Tag newTag) {
    return tagDao.insert(newTag);
  }

  @Override
  public ValidationObject isRightTag(TagDTO tagDTO) {
    boolean validId = tagDTO.getId() != null;
    if (!validId) return new ValidationObject("id of tag cannot be null", false);
    boolean validName = tagDTO.getName() != null && tagDTO.getName().length() <= 30;
    if (!validName)
      return new ValidationObject(
          "name of tag cannot be null or have more than 30 characters", false);
    return new ValidationObject("OK", true);
  }

  @Override
  public void associate(Integer tagId, Integer giftId) {

    tagDao.associate(tagId, giftId);
  }

  @Override
  public List<Tag> findByGiftId(Integer gift_id) {
    return tagDao.findByGiftId(gift_id);
  }

  @Override
  public List<Tag> findAll() {
    return tagDao.findAll();
  }

  @Override
  public Tag findById(Integer tag_id) {
    return tagDao.findById(tag_id);
  }

  @Override
  public void delete(Tag tag) {
    tagDao.deleteAssociationByTag(tag);
    tagDao.delete(tag);
  }
}
