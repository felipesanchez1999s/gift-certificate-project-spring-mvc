package com.epam.esm.service.impl;

import com.epam.esm.DTO.GiftDTO;
import com.epam.esm.dao.GiftDAO;
import com.epam.esm.dao.TagDao;
import com.epam.esm.models.Gift;
import com.epam.esm.service.GiftService;
import com.epam.esm.utils.Mapper;
import com.epam.esm.utils.ValidationObject;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class GiftServiceImpl implements GiftService {

  private GiftDAO giftDAO;
  private TagDao tagDao;

  public GiftServiceImpl(GiftDAO giftDAO, TagDao tagDao) {
    this.giftDAO = giftDAO;
    this.tagDao = tagDao;
  }

  @Override
  public Gift save(Gift newGiftDTO) {

    Timestamp now = Timestamp.from(Instant.now());

    newGiftDTO.setCreateDate(now);
    newGiftDTO.setLastUpdateDate(now);
    return giftDAO.insert(newGiftDTO);
  }

  @Override
  public boolean exist(Integer gift_id) {
    return giftDAO.findById(gift_id) != null;
  }

  @Override
  public ValidationObject isRightGift(GiftDTO giftDTO) {
    boolean validId = giftDTO.getId() != null;
    if (!validId) return new ValidationObject("id of gift cannot be null", false);
    boolean validName = giftDTO.getName() != null && giftDTO.getName().length() <= 30;
    if (!validName)
      return new ValidationObject(
          "name cannot be null and must have less than 30 characters", false);
    boolean validDescription =
        giftDTO.getDescription() != null && giftDTO.getDescription().length() <= 50;
    if (!validDescription)
      return new ValidationObject(
          "description cannot be null and must have less than 50 characters", false);
    boolean validPrice = giftDTO.getPrice() != null && giftDTO.getPrice().doubleValue() > 0;
    if (!validPrice)
      return new ValidationObject("price cannot be null and must be greater than 0", false);

    boolean validDuration = giftDTO.getDuration() != null && giftDTO.getDuration() > 0;
    if (!validDuration)
      return new ValidationObject("duration cannot be null and must be greater than 0", false);

    return new ValidationObject("OK", true);
  }

  @Override
  public ValidationObject isRigthForUpdate(GiftDTO giftDTO) {
    boolean validId = giftDTO.getId() != null;
    if (!validId) return new ValidationObject("id of gift cannot be null", false);
    boolean validName = giftDTO.getName() == null || giftDTO.getName().length() <= 30;
    if (!validName) return new ValidationObject("name  must have less than 30 characters", false);
    boolean validDescription =
        giftDTO.getDescription() == null || giftDTO.getDescription().length() <= 50;
    if (!validDescription)
      return new ValidationObject("description must have less than 50 characters", false);
    boolean validPrice = giftDTO.getPrice() == null || giftDTO.getPrice().doubleValue() > 0;
    if (!validPrice) return new ValidationObject("price must be greater than 0", false);

    boolean validDuration = giftDTO.getDuration() == null || giftDTO.getDuration() > 0;
    if (!validDuration) return new ValidationObject("duration  must be greater than 0", false);

    return new ValidationObject("OK", true);
  }

  @Override
  public List<GiftDTO> findAll() {
    List<Gift> gifts = giftDAO.findAll();

    return gifts.stream().map(gift -> Mapper.toGiftDTO(gift)).collect(Collectors.toList());
  }

  @Override
  public Gift findyById(Integer id) {
    return giftDAO.findById(id);
  }

  @Override
  public Gift update(GiftDTO giftDTO) {

    Gift oldGift = giftDAO.findById(giftDTO.getId());

    if (giftDTO.getName() != null) oldGift.setName(giftDTO.getName());
    if (giftDTO.getDescription() != null) oldGift.setDescription(giftDTO.getDescription());
    if (giftDTO.getPrice() != null) oldGift.setPrice(giftDTO.getPrice());
    if (giftDTO.getDuration() != null) oldGift.setDuration(giftDTO.getDuration());
    oldGift.setLastUpdateDate(Timestamp.from(Instant.now()));

    giftDAO.update(oldGift);

    return oldGift;
  }

  @Override
  public void delete(Gift gift) {
    tagDao.deleteAssociationByGift(gift);
    giftDAO.delete(gift);
  }

  @Override
  public List<GiftDTO> findByParams(
      String tagName,
      String partialName,
      String partialDescription,
      String sortField,
      String order) {
    List<Gift> giftList = null;
    if (tagName == null) giftList = giftDAO.findAll();
    else giftList = giftDAO.findByTagName(tagName);

    if (partialName != null)
      giftList =
          giftList.stream()
              .filter(gift -> gift.getName().contains(partialName))
              .collect(Collectors.toList());

    if (partialDescription != null)
      giftList =
          giftList.stream()
              .filter(gift -> gift.getDescription().contains(partialDescription))
              .collect(Collectors.toList());

    if (sortField != null) {
      giftList =
          giftList.stream()
              .sorted(
                  (o1, o2) -> {
                    Integer comparation = null;
                    if (sortField.equals("name"))
                      comparation = o1.getName().compareTo(o2.getName());
                    else if (sortField.equals("description"))
                      comparation = o1.getDescription().compareTo(o2.getDescription());
                    else if (sortField.equals("duration"))
                      comparation =
                          Integer.valueOf(o1.getDuration())
                              .compareTo(Integer.valueOf(o2.getDuration()));
                    else if (sortField.equals("price"))
                      comparation = o1.getPrice().compareTo(o2.getPrice());
                    else if (sortField.equals("create_date"))
                      comparation = o1.getCreateDate().compareTo(o2.getCreateDate());
                    else if (sortField.equals("last_update_date"))
                      comparation = o1.getLastUpdateDate().compareTo(o2.getLastUpdateDate());
                    else
                      comparation =
                          Integer.valueOf(o1.getId()).compareTo(Integer.valueOf(o2.getId()));

                    if (order != null && order.equals("DESC")) return -1 * comparation;
                    return comparation;
                  })
              .collect(Collectors.toList());
    }

    List<GiftDTO> giftDTOList =
        giftList.stream().map(gift -> Mapper.toGiftDTO(gift)).collect(Collectors.toList());
    giftDTOList.stream()
        .forEach(
            giftDTO -> {
              giftDTO.setTags(
                  tagDao.findByGiftId(giftDTO.getId()).stream()
                      .map(tag -> Mapper.toTagDTO(tag))
                      .collect(Collectors.toList()));
            });
    return giftDTOList;
  }
}
