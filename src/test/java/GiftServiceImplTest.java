import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.epam.esm.DTO.GiftDTO;
import com.epam.esm.DTO.TagDTO;
import com.epam.esm.dao.GiftDAO;
import com.epam.esm.dao.TagDao;
import com.epam.esm.models.Gift;
import com.epam.esm.service.impl.GiftServiceImpl;
import com.epam.esm.service.impl.TagServiceImpl;
import com.epam.esm.utils.ValidationObject;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class GiftServiceImplTest {

  @Mock private GiftDAO giftDAO;

  @Mock private TagDao tagDao;

  private GiftServiceImpl giftService;
  private TagServiceImpl tagService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
    giftService = new GiftServiceImpl(giftDAO, tagDao);
    tagService = new TagServiceImpl(tagDao);
  }

  @Test
  void testSave() {

    Gift newGiftDTO = new Gift();

    when(giftDAO.insert(newGiftDTO)).thenReturn(newGiftDTO);

    Gift savedGift = giftService.save(newGiftDTO);

    assertNotEquals(null, savedGift.getCreateDate());
    assertNotEquals(null, savedGift.getLastUpdateDate());

    verify(giftDAO).insert(newGiftDTO);
  }

  @Test
  void testExist_WhenGiftExists_ShouldReturnTrue() {

    int giftId = 1;
    Gift gift = new Gift();

    when(giftDAO.findById(giftId)).thenReturn(gift);

    boolean result = giftService.exist(giftId);

    assertTrue(result);

    verify(giftDAO).findById(giftId);
  }

  @Test
  void testExist_WhenGiftDoesNotExist_ShouldReturnFalse() {

    int giftId = 1;

    when(giftDAO.findById(giftId)).thenReturn(null);

    boolean result = giftService.exist(giftId);

    assertFalse(result);

    verify(giftDAO).findById(giftId);
  }

  @Test
  void testIsRightGift_WhenGiftDTOIsValid_ShouldReturnValidValidationObject() {

    GiftDTO giftDTO = new GiftDTO();
    giftDTO.setId(1);
    giftDTO.setName("Gift Name");
    giftDTO.setDescription("Gift Description");
    giftDTO.setPrice(new BigDecimal(10));
    giftDTO.setDuration(5);

    ValidationObject result = giftService.isRightGift(giftDTO);

    assertTrue(result.isValid());
    assertEquals("OK", result.getMessage());
  }

  @Test
  void testIsRightGift_WhenGiftDTOHasNullId_ShouldReturnInvalidValidationObject() {

    GiftDTO giftDTO = new GiftDTO();
    giftDTO.setName("Gift Name");
    giftDTO.setDescription("Gift Description");
    giftDTO.setPrice(new BigDecimal(10));
    giftDTO.setDuration(5);

    ValidationObject result = giftService.isRightGift(giftDTO);

    assertFalse(result.isValid());
    assertEquals("id of gift cannot be null", result.getMessage());
  }

  @Test
  void testFindByParams_WhenNoFilterApplied_ShouldReturnAllGifts() {
    Timestamp now = Timestamp.from(Instant.now());
    Gift gift1 = new Gift();
    gift1.setId(1);
    gift1.setName("Gift 1");
    gift1.setDescription("Description 1");
    gift1.setPrice(new BigDecimal(10));
    gift1.setDuration(5);
    gift1.setCreateDate(now);
    gift1.setLastUpdateDate(now);

    Gift gift2 = new Gift();
    gift2.setId(2);
    gift2.setName("Gift 2");
    gift2.setDescription("Description 2");
    gift2.setPrice(new BigDecimal(20));
    gift2.setDuration(10);
    gift2.setCreateDate(now);
    gift2.setLastUpdateDate(now);

    List<Gift> allGifts = Arrays.asList(gift1, gift2);

    when(giftDAO.findAll()).thenReturn(allGifts);

    List<GiftDTO> result = giftService.findByParams(null, null, null, null, null);

    assertEquals(2, result.size());
    assertEquals("Gift 1", result.get(0).getName());
    assertEquals("Gift 2", result.get(1).getName());
  }

  @Test
  void testFindByParams_WhenNoPartialNameApplied_ShouldReturnAllGifts() {
    Timestamp now = Timestamp.from(Instant.now());
    Gift gift1 = new Gift();
    gift1.setId(1);
    gift1.setName("Gift 1");
    gift1.setDescription("Description 1");
    gift1.setPrice(new BigDecimal(10));
    gift1.setDuration(5);
    gift1.setCreateDate(now);
    gift1.setLastUpdateDate(now);

    Gift gift2 = new Gift();
    gift2.setId(2);
    gift2.setName("Gift 2");
    gift2.setDescription("Description 2");
    gift2.setPrice(new BigDecimal(20));
    gift2.setDuration(10);
    gift2.setCreateDate(now);
    gift2.setLastUpdateDate(now);

    List<Gift> allGifts = Arrays.asList(gift1, gift2);

    when(giftDAO.findAll()).thenReturn(allGifts);

    List<GiftDTO> result = giftService.findByParams(null, "1", null, null, null);

    assertEquals(1, result.size());
    assertEquals("Gift 1", result.get(0).getName());
    verify(giftDAO).findAll();
  }

  @Test
  void testFindByParams_WhenPartialDescriptionApplied_ShouldReturnAllGifts() {
    Timestamp now = Timestamp.from(Instant.now());
    Gift gift1 = new Gift();
    gift1.setId(1);
    gift1.setName("Gift 1");
    gift1.setDescription("Description 1");
    gift1.setPrice(new BigDecimal(10));
    gift1.setDuration(5);
    gift1.setCreateDate(now);
    gift1.setLastUpdateDate(now);

    Gift gift2 = new Gift();
    gift2.setId(2);
    gift2.setName("Gift 2");
    gift2.setDescription("Description 2");
    gift2.setPrice(new BigDecimal(20));
    gift2.setDuration(10);
    gift2.setCreateDate(now);
    gift2.setLastUpdateDate(now);

    List<Gift> allGifts = Arrays.asList(gift1, gift2);

    when(giftDAO.findAll()).thenReturn(allGifts);

    List<GiftDTO> result = giftService.findByParams(null, null, "1", null, null);

    assertEquals(1, result.size());
    assertEquals("Gift 1", result.get(0).getName());
    verify(giftDAO).findAll();
  }

  @Test
  void testFindByParams_WhenSortFieldAppliedforName_ShouldReturnAllGifts() {
    Timestamp now = Timestamp.from(Instant.now());
    Gift gift1 = new Gift();
    gift1.setId(1);
    gift1.setName("Gift 1");
    gift1.setDescription("Description 1");
    gift1.setPrice(new BigDecimal(10));
    gift1.setDuration(5);
    gift1.setCreateDate(now);
    gift1.setLastUpdateDate(now);

    Gift gift2 = new Gift();
    gift2.setId(2);
    gift2.setName("Gift 2");
    gift2.setDescription("Description 2");
    gift2.setPrice(new BigDecimal(20));
    gift2.setDuration(10);
    gift2.setCreateDate(now);
    gift2.setLastUpdateDate(now);

    List<Gift> allGifts = Arrays.asList(gift1, gift2);

    when(giftDAO.findAll()).thenReturn(allGifts);

    List<GiftDTO> result = giftService.findByParams(null, null, null, "name", null);

    assertEquals(2, result.size());
    assertEquals("Gift 1", result.get(0).getName());
    assertEquals("Gift 2", result.get(1).getName());
    verify(giftDAO).findAll();
  }

  @Test
  void testFindByParams_WhenSortFieldAppliedforDescription_ShouldReturnAllGifts() {
    Timestamp now = Timestamp.from(Instant.now());
    Gift gift1 = new Gift();
    gift1.setId(1);
    gift1.setName("Gift 1");
    gift1.setDescription("Description 1");
    gift1.setPrice(new BigDecimal(10));
    gift1.setDuration(5);
    gift1.setCreateDate(now);
    gift1.setLastUpdateDate(now);

    Gift gift2 = new Gift();
    gift2.setId(2);
    gift2.setName("Gift 2");
    gift2.setDescription("Description 2");
    gift2.setPrice(new BigDecimal(20));
    gift2.setDuration(10);
    gift2.setCreateDate(now);
    gift2.setLastUpdateDate(now);

    List<Gift> allGifts = Arrays.asList(gift1, gift2);

    when(giftDAO.findAll()).thenReturn(allGifts);

    List<GiftDTO> result = giftService.findByParams(null, null, null, "description", null);

    assertEquals(2, result.size());
    assertEquals("Gift 1", result.get(0).getName());
    assertEquals("Gift 2", result.get(1).getName());
    verify(giftDAO).findAll();
  }

  @Test
  void testFindByParams_WhenSortFieldAppliedforDuration_ShouldReturnAllGifts() {
    Timestamp now = Timestamp.from(Instant.now());
    Gift gift1 = new Gift();
    gift1.setId(1);
    gift1.setName("Gift 1");
    gift1.setDescription("Description 1");
    gift1.setPrice(new BigDecimal(10));
    gift1.setDuration(5);
    gift1.setCreateDate(now);
    gift1.setLastUpdateDate(now);

    Gift gift2 = new Gift();
    gift2.setId(2);
    gift2.setName("Gift 2");
    gift2.setDescription("Description 2");
    gift2.setPrice(new BigDecimal(20));
    gift2.setDuration(10);
    gift2.setCreateDate(now);
    gift2.setLastUpdateDate(now);

    List<Gift> allGifts = Arrays.asList(gift1, gift2);

    when(giftDAO.findAll()).thenReturn(allGifts);

    List<GiftDTO> result = giftService.findByParams(null, null, null, "duration", null);

    assertEquals(2, result.size());
    assertEquals("Gift 1", result.get(0).getName());
    assertEquals("Gift 2", result.get(1).getName());
    verify(giftDAO).findAll();
  }

  @Test
  void testFindByParams_WhenSortFieldAppliedforPrice_ShouldReturnAllGifts() {
    Timestamp now = Timestamp.from(Instant.now());
    Gift gift1 = new Gift();
    gift1.setId(1);
    gift1.setName("Gift 1");
    gift1.setDescription("Description 1");
    gift1.setPrice(new BigDecimal(10));
    gift1.setDuration(5);
    gift1.setCreateDate(now);
    gift1.setLastUpdateDate(now);

    Gift gift2 = new Gift();
    gift2.setId(2);
    gift2.setName("Gift 2");
    gift2.setDescription("Description 2");
    gift2.setPrice(new BigDecimal(20));
    gift2.setDuration(10);
    gift2.setCreateDate(now);
    gift2.setLastUpdateDate(now);

    List<Gift> allGifts = Arrays.asList(gift1, gift2);

    when(giftDAO.findAll()).thenReturn(allGifts);

    List<GiftDTO> result = giftService.findByParams(null, null, null, "price", null);

    assertEquals(2, result.size());
    assertEquals("Gift 1", result.get(0).getName());
    assertEquals("Gift 2", result.get(1).getName());
    verify(giftDAO).findAll();
  }

  @Test
  void testFindByParams_WhenSortFieldAppliedforcreateDate_ShouldReturnAllGifts() {
    Timestamp now = Timestamp.from(Instant.now());
    Gift gift1 = new Gift();
    gift1.setId(1);
    gift1.setName("Gift 1");
    gift1.setDescription("Description 1");
    gift1.setPrice(new BigDecimal(10));
    gift1.setDuration(5);
    gift1.setCreateDate(now);
    gift1.setLastUpdateDate(now);

    Gift gift2 = new Gift();
    gift2.setId(2);
    gift2.setName("Gift 2");
    gift2.setDescription("Description 2");
    gift2.setPrice(new BigDecimal(20));
    gift2.setDuration(10);
    gift2.setCreateDate(now);
    gift2.setLastUpdateDate(now);

    List<Gift> allGifts = Arrays.asList(gift1, gift2);

    when(giftDAO.findAll()).thenReturn(allGifts);

    List<GiftDTO> result = giftService.findByParams(null, null, null, "create_date", null);

    assertEquals(2, result.size());
    assertEquals("Gift 1", result.get(0).getName());
    assertEquals("Gift 2", result.get(1).getName());
    verify(giftDAO).findAll();
  }

  @Test
  void testFindByParams_WhenSortFieldAppliedforLastUpdateDate_ShouldReturnAllGifts() {
    Timestamp now = Timestamp.from(Instant.now());
    Gift gift1 = new Gift();
    gift1.setId(1);
    gift1.setName("Gift 1");
    gift1.setDescription("Description 1");
    gift1.setPrice(new BigDecimal(10));
    gift1.setDuration(5);
    gift1.setCreateDate(now);
    gift1.setLastUpdateDate(now);

    Gift gift2 = new Gift();
    gift2.setId(2);
    gift2.setName("Gift 2");
    gift2.setDescription("Description 2");
    gift2.setPrice(new BigDecimal(20));
    gift2.setDuration(10);
    gift2.setCreateDate(now);
    gift2.setLastUpdateDate(now);

    List<Gift> allGifts = Arrays.asList(gift1, gift2);

    when(giftDAO.findAll()).thenReturn(allGifts);

    List<GiftDTO> result = giftService.findByParams(null, null, null, "last_update_date", null);

    assertEquals(2, result.size());
    assertEquals("Gift 1", result.get(0).getName());
    assertEquals("Gift 2", result.get(1).getName());
    verify(giftDAO).findAll();
  }

  @Test
  void testFindByParams_WhenSortFieldAppliedforId_ShouldReturnAllGifts() {
    Timestamp now = Timestamp.from(Instant.now());
    Gift gift1 = new Gift();
    gift1.setId(1);
    gift1.setName("Gift 1");
    gift1.setDescription("Description 1");
    gift1.setPrice(new BigDecimal(10));
    gift1.setDuration(5);
    gift1.setCreateDate(now);
    gift1.setLastUpdateDate(now);

    Gift gift2 = new Gift();
    gift2.setId(2);
    gift2.setName("Gift 2");
    gift2.setDescription("Description 2");
    gift2.setPrice(new BigDecimal(20));
    gift2.setDuration(10);
    gift2.setCreateDate(now);
    gift2.setLastUpdateDate(now);

    List<Gift> allGifts = Arrays.asList(gift1, gift2);

    when(giftDAO.findAll()).thenReturn(allGifts);

    List<GiftDTO> result = giftService.findByParams(null, null, null, "id", null);

    assertEquals(2, result.size());
    assertEquals("Gift 1", result.get(0).getName());
    assertEquals("Gift 2", result.get(1).getName());
    verify(giftDAO).findAll();
  }

  @Test
  void testFindByParams_WhenSortFieldAppliedforIdDESC_ShouldReturnAllGifts() {
    Timestamp now = Timestamp.from(Instant.now());
    Gift gift1 = new Gift();
    gift1.setId(1);
    gift1.setName("Gift 1");
    gift1.setDescription("Description 1");
    gift1.setPrice(new BigDecimal(10));
    gift1.setDuration(5);
    gift1.setCreateDate(now);
    gift1.setLastUpdateDate(now);

    Gift gift2 = new Gift();
    gift2.setId(2);
    gift2.setName("Gift 2");
    gift2.setDescription("Description 2");
    gift2.setPrice(new BigDecimal(20));
    gift2.setDuration(10);
    gift2.setCreateDate(now);
    gift2.setLastUpdateDate(now);

    List<Gift> allGifts = Arrays.asList(gift1, gift2);

    when(giftDAO.findAll()).thenReturn(allGifts);

    List<GiftDTO> result = giftService.findByParams(null, null, null, "id", "DESC");

    assertEquals(2, result.size());
    assertEquals("Gift 1", result.get(1).getName());
    assertEquals("Gift 2", result.get(0).getName());
    verify(giftDAO).findAll();
  }

  @Test
  public void testIsRightForUpdate_ValidGiftDTO_ReturnsOkValidationObject() {
    GiftDTO giftDTO = mock(GiftDTO.class);
    when(giftDTO.getId()).thenReturn(1);
    when(giftDTO.getName()).thenReturn("Valid Name");
    when(giftDTO.getDescription()).thenReturn("Valid Description");
    when(giftDTO.getPrice()).thenReturn(BigDecimal.valueOf(10.0));
    when(giftDTO.getDuration()).thenReturn(5);

    ValidationObject result = giftService.isRigthForUpdate(giftDTO);

    assertTrue(result.isValid());
    assertEquals("OK", result.getMessage());
  }

  @Test
  public void testIsRightForUpdate_NullId_ReturnsInvalidValidationObject() {
    GiftDTO giftDTO = mock(GiftDTO.class);
    when(giftDTO.getId()).thenReturn(null);

    ValidationObject result = giftService.isRigthForUpdate(giftDTO);

    assertFalse(result.isValid());
    assertEquals("id of gift cannot be null", result.getMessage());
  }

  @Test
  public void testIsRightForUpdate_InvalidNameLength_ReturnsInvalidValidationObject() {
    GiftDTO giftDTO = mock(GiftDTO.class);
    when(giftDTO.getId()).thenReturn(1);
    when(giftDTO.getName())
        .thenReturn("This is a very long name that exceeds the maximum length allowed");

    ValidationObject result = giftService.isRigthForUpdate(giftDTO);

    assertFalse(result.isValid());
    assertEquals("name  must have less than 30 characters", result.getMessage());
  }

  @Test
  public void testIsRightForUpdate_InvalidDescriptionLength_ReturnsInvalidValidationObject() {
    GiftDTO giftDTO = mock(GiftDTO.class);
    when(giftDTO.getId()).thenReturn(1);
    when(giftDTO.getDescription())
        .thenReturn(
            "This is a very long description that exceeds the maximum length allowed for descriptions in the system");

    ValidationObject result = giftService.isRigthForUpdate(giftDTO);

    assertFalse(result.isValid());
    assertEquals("description must have less than 50 characters", result.getMessage());
  }

  @Test
  public void testIsRightForUpdate_InvalidPrice_ReturnsInvalidValidationObject() {
    GiftDTO giftDTO = mock(GiftDTO.class);
    when(giftDTO.getId()).thenReturn(1);
    when(giftDTO.getPrice()).thenReturn(BigDecimal.valueOf(0.0));

    ValidationObject result = giftService.isRigthForUpdate(giftDTO);

    assertFalse(result.isValid());
    assertEquals("price must be greater than 0", result.getMessage());
  }

  @Test
  public void testIsRightForUpdate_InvalidDuration_ReturnsInvalidValidationObject() {
    GiftDTO giftDTO = mock(GiftDTO.class);
    when(giftDTO.getId()).thenReturn(1);
    when(giftDTO.getDuration()).thenReturn(-5);

    ValidationObject result = giftService.isRigthForUpdate(giftDTO);

    assertFalse(result.isValid());
    assertEquals("duration  must be greater than 0", result.getMessage());
  }

  @Test
  public void testIsRightGift_ValidGiftDTO_ReturnsOkValidationObject() {
    GiftDTO giftDTO = mock(GiftDTO.class);
    when(giftDTO.getId()).thenReturn(1);
    when(giftDTO.getName()).thenReturn("Valid Name");
    when(giftDTO.getDescription()).thenReturn("Valid Description");
    when(giftDTO.getPrice()).thenReturn(BigDecimal.valueOf(10.0));
    when(giftDTO.getDuration()).thenReturn(5);

    ValidationObject result = giftService.isRightGift(giftDTO);

    assertTrue(result.isValid());
    assertEquals("OK", result.getMessage());
  }

  @Test
  public void testIsRightGift_NullId_ReturnsInvalidValidationObject() {
    GiftDTO giftDTO = mock(GiftDTO.class);
    when(giftDTO.getId()).thenReturn(null);

    ValidationObject result = giftService.isRightGift(giftDTO);

    assertFalse(result.isValid());
    assertEquals("id of gift cannot be null", result.getMessage());
  }

  @Test
  public void testIsRightGift_NullName_ReturnsInvalidValidationObject() {
    GiftDTO giftDTO = mock(GiftDTO.class);
    when(giftDTO.getId()).thenReturn(1);
    when(giftDTO.getName()).thenReturn(null);

    ValidationObject result = giftService.isRightGift(giftDTO);

    assertFalse(result.isValid());
    assertEquals("name cannot be null and must have less than 30 characters", result.getMessage());
  }

  @Test
  public void testIsRightGift_InvalidNameLength_ReturnsInvalidValidationObject() {
    GiftDTO giftDTO = mock(GiftDTO.class);
    when(giftDTO.getId()).thenReturn(1);
    when(giftDTO.getName())
        .thenReturn("This is a very long name that exceeds the maximum length allowed");

    ValidationObject result = giftService.isRightGift(giftDTO);

    assertFalse(result.isValid());
    assertEquals("name cannot be null and must have less than 30 characters", result.getMessage());
  }

  @Test
  public void testIsRightGift_NullDescription_ReturnsInvalidValidationObject() {
    GiftDTO giftDTO = mock(GiftDTO.class);
    when(giftDTO.getId()).thenReturn(1);
    when(giftDTO.getName()).thenReturn("Valid Name");
    when(giftDTO.getDescription()).thenReturn(null);

    ValidationObject result = giftService.isRightGift(giftDTO);

    assertFalse(result.isValid());
    assertEquals(
        "description cannot be null and must have less than 50 characters", result.getMessage());
  }

  @Test
  public void testIsRightGift_InvalidDescriptionLength_ReturnsInvalidValidationObject() {
    GiftDTO giftDTO = mock(GiftDTO.class);
    when(giftDTO.getId()).thenReturn(1);
    when(giftDTO.getName()).thenReturn("Valid Name");
    when(giftDTO.getDescription())
        .thenReturn(
            "This is a very long description that exceeds the maximum length allowed for descriptions in the system");

    ValidationObject result = giftService.isRightGift(giftDTO);

    assertFalse(result.isValid());
    assertEquals(
        "description cannot be null and must have less than 50 characters", result.getMessage());
  }

  @Test
  public void testIsRightGift_NullPrice_ReturnsInvalidValidationObject() {
    GiftDTO giftDTO = mock(GiftDTO.class);
    when(giftDTO.getId()).thenReturn(1);
    when(giftDTO.getName()).thenReturn("Valid Name");
    when(giftDTO.getDescription()).thenReturn("Valid Description");
    when(giftDTO.getPrice()).thenReturn(null);

    ValidationObject result = giftService.isRightGift(giftDTO);

    assertFalse(result.isValid());
    assertEquals("price cannot be null and must be greater than 0", result.getMessage());
  }

  @Test
  public void testIsRightGift_InvalidPrice_ReturnsInvalidValidationObject() {
    GiftDTO giftDTO = mock(GiftDTO.class);
    when(giftDTO.getId()).thenReturn(1);
    when(giftDTO.getName()).thenReturn("Valid Name");
    when(giftDTO.getDescription()).thenReturn("Valid Description");
    when(giftDTO.getPrice()).thenReturn(BigDecimal.valueOf(0.0));

    ValidationObject result = giftService.isRightGift(giftDTO);

    assertFalse(result.isValid());
    assertEquals("price cannot be null and must be greater than 0", result.getMessage());
  }

  @Test
  public void testIsRightGift_NullDuration_ReturnsInvalidValidationObject() {
    GiftDTO giftDTO = mock(GiftDTO.class);
    when(giftDTO.getId()).thenReturn(1);
    when(giftDTO.getName()).thenReturn("Valid Name");
    when(giftDTO.getDescription()).thenReturn("Valid Description");
    when(giftDTO.getPrice()).thenReturn(BigDecimal.valueOf(10.0));
    when(giftDTO.getDuration()).thenReturn(null);

    ValidationObject result = giftService.isRightGift(giftDTO);

    assertFalse(result.isValid());
    assertEquals("duration cannot be null and must be greater than 0", result.getMessage());
  }

  @Test
  public void testUpdate_ValidGiftDTO_ReturnsUpdatedGift() {
    // Mock data
    int giftId = 1;
    String updatedName = "Updated Name";
    String updatedDescription = "Updated Description";
    Double updatedPrice = 100.0;
    Integer updatedDuration = 10;
    Timestamp expectedLastUpdateDate = Timestamp.from(Instant.now());

    GiftDTO giftDTO = new GiftDTO();
    giftDTO.setId(giftId);
    giftDTO.setName(updatedName);
    giftDTO.setDescription(updatedDescription);
    giftDTO.setPrice(BigDecimal.valueOf(updatedPrice));
    giftDTO.setDuration(updatedDuration);

    Gift oldGift = new Gift();
    oldGift.setId(giftId);
    oldGift.setName("Old Name");
    oldGift.setDescription("Old Description");
    oldGift.setPrice(BigDecimal.valueOf(50.0));
    oldGift.setDuration(5);

    when(giftDAO.findById(giftId)).thenReturn(oldGift);

    Gift updatedGift = giftService.update(giftDTO);

    // Verify the updated gift object
    assertEquals(giftId, updatedGift.getId());
    assertEquals(updatedName, updatedGift.getName());
    assertEquals(updatedDescription, updatedGift.getDescription());
    assertEquals(updatedPrice, updatedGift.getPrice().doubleValue());
    assertEquals(updatedDuration, updatedGift.getDuration());

    // Verify that the giftDAO.update method was called with the updated gift
    verify(giftDAO, times(1)).update(updatedGift);
  }

  @Test
  public void testIsRightTag_ValidTagDTO_ReturnsValidValidationObject() {
    // Test data
    TagDTO tagDTO = new TagDTO();
    tagDTO.setId(1);
    tagDTO.setName("ValidTag");

    ValidationObject validationObject = tagService.isRightTag(tagDTO);

    // Verify the validation object
    assertTrue(validationObject.isValid());
    assertEquals("OK", validationObject.getMessage());
  }

  @Test
  public void testIsRightTag_NullId_ReturnsInvalidValidationObject() {
    // Test data
    TagDTO tagDTO = new TagDTO();
    tagDTO.setId(null);
    tagDTO.setName("ValidTag");

    ValidationObject validationObject = tagService.isRightTag(tagDTO);

    // Verify the validation object
    assertFalse(validationObject.isValid());
    assertEquals("id of tag cannot be null", validationObject.getMessage());
  }

  @Test
  public void testIsRightTag_NullName_ReturnsInvalidValidationObject() {
    // Test data
    TagDTO tagDTO = new TagDTO();
    tagDTO.setId(1);
    tagDTO.setName(null);

    ValidationObject validationObject = tagService.isRightTag(tagDTO);

    // Verify the validation object
    assertFalse(validationObject.isValid());
    assertEquals(
        "name of tag cannot be null or have more than 30 characters",
        validationObject.getMessage());
  }

  @Test
  public void testIsRightTag_LongName_ReturnsInvalidValidationObject() {
    // Test data
    TagDTO tagDTO = new TagDTO();
    tagDTO.setId(1);
    tagDTO.setName("ThisIsAReallyLongTagNameThatExceedsTheCharacterLimit");

    ValidationObject validationObject = tagService.isRightTag(tagDTO);

    // Verify the validation object
    assertFalse(validationObject.isValid());
    assertEquals(
        "name of tag cannot be null or have more than 30 characters",
        validationObject.getMessage());
  }
}
