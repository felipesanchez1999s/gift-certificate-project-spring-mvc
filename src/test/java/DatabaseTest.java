import com.epam.esm.dao.GiftDAO;
import com.epam.esm.dao.TagDao;
import com.epam.esm.dao.impl.JdbcTemplateGiftDAO;
import com.epam.esm.models.Gift;
import com.epam.esm.models.Tag;
import java.math.BigDecimal;
import java.sql.*;
import java.time.Instant;
import java.util.List;
import javax.sql.DataSource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class, JdbcTemplateGiftDAO.class})
public class DatabaseTest {
  @Autowired private DataSource dataSource;
  @Autowired GiftDAO giftDAO;
  @Autowired TagDao tagDao;

  private static String INIT_DATABASE =
      ""
          + "DROP TABLE IF EXISTS gift_tag;\n"
          + "DROP TABLE IF EXISTS gift_certificate;\n"
          + "CREATE TABLE gift_certificate (\n"
          + "       gift_id\t\t      int not null,\n"
          + "       name\t\t      varchar(30),\n"
          + "       description \t      varchar(50),\n"
          + "       price\t\t      decimal,\n"
          + "       duration\t\t      int,\n"
          + "       create_date\t      timestamp,\n"
          + "       last_update_date\t      timestamp,\n"
          + "       PRIMARY KEY(gift_id)\n"
          + ");\n"
          + "DROP TABLE IF EXISTS tag;\n"
          + "CREATE TABLE tag (\n"
          + "       tag_id\t\t      int not null,\n"
          + "       name\t\t      varchar(30),\n"
          + "       PRIMARY KEY(tag_id) \n"
          + ");\n"
          + "CREATE TABLE gift_tag (\n"
          + "       gift_id\t\t      int not null,\n"
          + "       tag_id\t\t      int not null,\n"
          + "       PRIMARY KEY(gift_id, tag_id),\n"
          + "       FOREIGN KEY(gift_id) REFERENCES gift_certificate(gift_id) ,\n"
          + "       FOREIGN KEY(tag_id)  REFERENCES tag(tag_id)\n"
          + ");\n";

  @Before
  public void setUp() {
    Gift gift = null;
    try (Connection conn = dataSource.getConnection();
        PreparedStatement ps = conn.prepareStatement(INIT_DATABASE)) {

      ps.executeUpdate();

    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Test
  public void insertTest() {
    Gift newGift = new Gift();
    newGift.setId(1);
    newGift.setName("Gift 1");
    newGift.setDescription("desc1");
    newGift.setPrice(BigDecimal.valueOf(10l));
    newGift.setDuration(10);
    newGift.setCreateDate(Timestamp.from(Instant.now()));
    newGift.setLastUpdateDate(Timestamp.from(Instant.now()));

    giftDAO.insert(newGift);
    Gift retrievedGift = giftDAO.findById(newGift.getId());
    Assert.assertEquals(retrievedGift.getId(), newGift.getId());
    Assert.assertEquals(retrievedGift.getName(), newGift.getName());
  }

  @Test
  public void insertTest2() {
    Gift newGift = new Gift();
    newGift.setId(1);
    newGift.setName("Gift 2");
    newGift.setDescription("desc1");
    newGift.setPrice(BigDecimal.valueOf(10l));
    newGift.setDuration(10);
    newGift.setCreateDate(Timestamp.from(Instant.now()));
    newGift.setLastUpdateDate(Timestamp.from(Instant.now()));

    giftDAO.insert(newGift);
    Gift retrievedGift = giftDAO.findById(newGift.getId());
    Assert.assertEquals(retrievedGift.getId(), newGift.getId());
    Assert.assertEquals(retrievedGift.getName(), newGift.getName());
  }

  @Test
  public void update() {
    Gift newGift = new Gift();
    newGift.setId(1);
    newGift.setName("Gift 1");
    newGift.setDescription("desc1");
    newGift.setPrice(BigDecimal.valueOf(10l));
    newGift.setDuration(10);
    newGift.setCreateDate(Timestamp.from(Instant.now()));
    newGift.setLastUpdateDate(Timestamp.from(Instant.now()));

    giftDAO.insert(newGift);
    Gift retrievedGift = giftDAO.findById(newGift.getId());
    Assert.assertEquals(retrievedGift.getId(), newGift.getId());
    Assert.assertEquals(retrievedGift.getName(), newGift.getName());
    newGift.setName("changedName");
    giftDAO.update(newGift);
    retrievedGift = giftDAO.findById(newGift.getId());
    Assert.assertEquals(retrievedGift.getName(), newGift.getName());
  }

  @Test
  public void delete() {
    Gift newGift = new Gift();
    newGift.setId(1);
    newGift.setName("Gift 1");
    newGift.setDescription("desc1");
    newGift.setPrice(BigDecimal.valueOf(10l));
    newGift.setDuration(10);
    newGift.setCreateDate(Timestamp.from(Instant.now()));
    newGift.setLastUpdateDate(Timestamp.from(Instant.now()));

    giftDAO.insert(newGift);
    List<Gift> gifts = giftDAO.findAll();
    Assert.assertEquals(gifts.size(), 1);
    giftDAO.delete(newGift);
    gifts = giftDAO.findAll();
    Assert.assertEquals(gifts.size(), 0);
  }

  @Test
  public void findByTagName() {
    Gift newGift = new Gift();
    newGift.setId(1);
    newGift.setName("Gift 1");
    newGift.setDescription("desc1");
    newGift.setPrice(BigDecimal.valueOf(10l));
    newGift.setDuration(10);
    newGift.setCreateDate(Timestamp.from(Instant.now()));
    newGift.setLastUpdateDate(Timestamp.from(Instant.now()));

    giftDAO.insert(newGift);

    Tag tag = new Tag();
    tag.setId(1);
    tag.setName("tag 1");
    tagDao.insert(tag);
    tagDao.associate(tag.getId(), newGift.getId());

    List<Gift> gifts = giftDAO.findByTagName(tag.getName());
    Assert.assertEquals(gifts.size(), 1);
    Assert.assertEquals(gifts.get(0).getName(), newGift.getName());
  }
}
